```
Usage of cloudSubmit:
  -archive
        If truthy, grids will be stored in the and ingested using the value from -bucket option or the default archive s3 bucket, "cag-grid-data-archive-us-east-1"
  -archive-only
        If truthy, grids will not be ingested but only put in to the value from -bucket option or default archive s3 bucket, "cag-grid-data-archive-us-east-1"
  -base-path string
        prepend to blob name from notifier event
  -bucket string
        S3 Bucket name where gridded data is put. Data put in the s3 bucket will use the bucket's lifecycle rules. Setting this will override the default bucket when using the -submit_dc argument.
  -config_dir string
        directory that app searches to find the configuration file. (default "/Users/tleben/devel/cloudingest/cloudsubmit-release//conf")
  -config_target string
        filename that app looks for to provide the config values. (default "config.yaml")
  -dryrun
        used for debug/testing. Does not ingest data or put data in s3 bucket
        directory that app searches to find the configuration file. (default "/Users/tleben/devel/cloudingest/cloudsubmit-release//conf")                                                                                             [5/3868]
  -config_target string
        filename that app looks for to provide the config values. (default "config.yaml")
  -dryrun
        used for debug/testing. Does not ingest data or put data in s3 bucket
  -keyspace string
        wxgrid_long_lived_v2 | soil_model_v5 (default "DEBUG")
  -manual_submission
        pass along that we are a manual submission
  -nocompress
        do not compress gridded data with zlib.
        **Uncompressed data is not supported yet in the downstream ingest writer process**
  -notifier-from-file string
        path to notifier file (default "/notifier_file")
  -notifier_event string
        single event string following the notifier format, takes precedence over notifier-from-file
  -priority int
        Sets the priority for this submission, range 0-10 (default 5)
  -region string
        region that we want to send messages to (eg, US or EU). Defaults to ALL (default "ALL")
  -rmq_exchange string
        Rabbitmq exchange (default "wxgrid.ha.notifier_header_exchange")
  -rmq_host string
        single rabbitMQ host (default "internal-rabbitmq-c1-940220365.us-east-1.elb.amazonaws.com")
  -rmq_password string
        Rabbitmq password (default "wxgrid")
  -rmq_port int
        rabbitMQ port, default should be fine in most cases (default 5672)
  -rmq_user string
        Rabbitmq username (default "wxgrid")
  -rmq_vhost string
        Rabbitmq virtual host (default "wxgrid")
  -s3Region string
        S3 Region (default "us-east-1")
  -s3Timeout duration
        S3 upload timeout. (default 30s)
  -s3_profile string
        Target AWS role to use from configuration
  -skip_s3
        data will still be ingested but it will not be put into the configured s3 bucket.
        If the data does not exist in the s3 bucket, the grid could potentially be written as all missing values
  -stacking_rule string
        the stacking rule for notifier files (current, hourly, or daily) (default "current")
  -submit_dc string
        Uses internal default for rabbitMQ host & s3 bucket. Allowed [qarth, pyke, braavos].
        If -bucket is set, that value will be used instead of the default s3 bucket
  -submitting_program string
        name of the program/process that is submitting the grids (default "goSubmit")
  -ttl int
        data lifetime in the database
  -workers int
        number of concurrent workers that are uploading files to s3 (default 30)
```
